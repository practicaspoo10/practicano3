package ito.poo.app;

import java.time.LocalDate;

import ito.poo.clases.CuentaBancaria;

public class MyApp {
	
	static void run() {
		CuentaBancaria CB;
		
		CB= new CuentaBancaria();
		
		System.out.println(CB);
		
		LocalDate fecha=LocalDate.now();
		
		System.out.println(new CuentaBancaria(12450,"Alondra Martinez",2300f,null));
		System.out.println(new CuentaBancaria(19541,"Maria Prado",75410f, null));
		System.out.println(new CuentaBancaria(21540,"Ximena Trejo",24510f, null));
		System.out.println(new CuentaBancaria(10254,"Karla Zu�iga",35621f, fecha));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
